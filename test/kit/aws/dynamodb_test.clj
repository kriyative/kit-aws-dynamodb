(ns kit.aws.dynamodb-test
  (:require
   [clojure.test :refer :all]
   [kit.log :as log]
   [kit.aws.dynamodb :as ddb]))

(deftest transact-write-items
  (let [state (atom [])]
    (with-redefs [ddb/invoke-request (fn invoke-request
                                       ([op req]
                                        (invoke-request op req {}))
                                       ([op req opts]
                                        (swap! state conj {:op op
                                                           :req req
                                                           :opts opts})))]
      (ddb/transact-write-items
       {:transact-items
        [{:Put {:TableName :testTable
                :Item {:id "ident-1" :name "test-name"}}}
         {:Delete {:TableName :testTable
                   :Key {:id "ident-2"}}}]})
      (is (= [{:op :transact-write-items,
               :req
               {:transact-items
                [{:Put
                  {:TableName :testTable,
                   :Item {:id {:S "ident-1"}, :name {:S "test-name"}}}}
                 {:Delete {:TableName :testTable, :Key {:id {:S "ident-2"}}}}]},
               :opts {}}]
             @state)))))
