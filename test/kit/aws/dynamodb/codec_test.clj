(ns kit.aws.dynamodb.codec-test
  (:require
   [clojure.test :refer :all]
   [kit.aws.dynamodb.codec :as codec]))

(deftest encode
  (is (= {:NULL true} (codec/encode-value nil)))
  (is (= {:BOOL false} (codec/encode-value false)))
  (is (= {:BOOL true} (codec/encode-value true)))
  (is (= {:N "123"} (codec/encode-value 123)))
  (is (= {:N "123.45"} (codec/encode-value 123.45)))
  (is (= {:S "5474972"} (codec/encode-value "5474972")))
  (is (= {:L [{:S "#clj :foo"} {:S "bar"} {:N "123"}]}
         (codec/encode-value [:foo "bar" 123])))
  (is (= {:M {:name {:S "buy-n-large"}
              :active {:BOOL true}
              :language {:S "#clj :en"}
              :source {:S "#clj :google"}
              :amount {:N "123"}
              :tags {:SS ["#clj :organization" "#clj :retail"]}}}
         (codec/encode-value
          {:name "buy-n-large"
           :active true
           :language :en
           :source :google
           :amount 123
           :tags #{:organization :retail}})))
  (is (= {:SS ["#clj \"foo\"" "#clj \"bar\"" "#clj 123"]}
         (codec/encode-value #{"foo" "bar" 123})))
  (is (= {:S "#clj :foo"} (codec/encode-value :foo)))
  (is (= {:S "#clj foo"} (codec/encode-value 'foo)))
  (is (= {:S "#clj \"#clj :foo\""} (codec/encode-value "#clj :foo")))
  (is (= #{"foo" "bar" 123}
         (codec/decode-value (codec/encode-value #{"foo" "bar" 123})))))

(deftest decode
  (is (= nil (codec/decode-value {:NULL true})))
  (is (= false (codec/decode-value {:BOOL false})))
  (is (= true (codec/decode-value {:BOOL true})))
  (is (= 123 (codec/decode-value {:N "123"})))
  (is (= 123.45 (codec/decode-value {:N "123.45"})))
  (is (= "5474972" (codec/decode-value {"S" "5474972"})))
  (is (= [:foo "bar" 123]
         (codec/decode-value {:L [{:S "#clj :foo"} {:S "bar"} {:N "123"}]})))
  (is (= {:name "buy-n-large"
          :active true
          :language :en
          :source :google
          :amount 123
          :tags #{:organization :retail}}
         (codec/decode-value
          {:M {:name {:S "buy-n-large"}
               :active {:BOOL true}
               :language {:S "#clj :en"}
               :source {:S "#clj :google"}
               :amount {:N "123"}
               :tags {:SS ["#clj :organization" "#clj :retail"]}}})))
  (is (= #{"foo" "bar" 123}
         (codec/decode-value {:SS ["foo" "bar" "#clj 123"]})))
  (is (= :foo (codec/decode-value {:S "#clj :foo"})))
  (is (= 'foo (codec/decode-value {:S "#clj foo"})))
  (is (= "#clj :foo" (codec/decode-value {:S "#clj \"#clj :foo\""}))))
