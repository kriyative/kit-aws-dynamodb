(ns kit.aws.dynamodb
  (:require
   [cognitect.anomalies :as ca]
   [kit.util :as u]
   [kit.aws.core :as ac]
   [kit.aws.dynamodb.codec :as codec]))

(defonce client (atom nil))

(defn init!
  ([]
   (init! {}))
  ([conf]
   (reset! client (ac/make-client
                   (merge {:api :dynamodb} conf)))))

;; (reset! client nil)
;; (init! {})

(defn list-tables []
  (ac/invoke @client :list-tables))

;; (list-tables)

(defn update-if-contains? [m k f & args]
  (if (contains? m k)
    (apply update m k f args)
    m))

(defn update-with-codec [codec object ks]
  (reduce (fn [object k]
            (update-if-contains? object k codec))
          object
          ks))

(defn invoke-request
  ([op request]
   (invoke-request op request {}))
  ([op request {:keys [encode-attribs decode-attribs]}]
   (let [request (update-with-codec codec/encode
                                    request
                                    (concat [:expression-attribute-values]
                                            encode-attribs))]
     (update-with-codec codec/decode
                        (ac/invoke @client op request)
                        (concat [:items] decode-attribs)))))

(defn scan [request]
  (invoke-request :scan request))

(defn scan-seq-basic [request collection-key]
  (u/scrolling-seq
   (fn [start-key]
     (let [result (scan
                   (merge request
                          (when start-key
                            {:exclusive-start-key start-key})))
           {:keys [last-evaluated-key]} result]
       [(u/as-coll (collection-key result))
        (not-empty last-evaluated-key)]))))

(defn scan-seq [request]
  (scan-seq-basic request :items))

(defn scan-seq-count [request]
  (reduce + 0 (scan-seq-basic request :count)))

(defn query [request]
  (invoke-request :query request))

(defn query-seq-basic [request collection-key]
  (u/scrolling-seq
   (fn [start-key]
     (let [result (query
                   (merge request
                          (when start-key
                            {:exclusive-start-key start-key})))
           {:keys [last-evaluated-key]} result]
       [(u/as-coll (collection-key result))
        (not-empty last-evaluated-key)]))))

(defn query-seq [request]
  (query-seq-basic request :items))

(defn query-seq-count [request]
  (reduce +
          0
          (query-seq-basic (merge request
                                  {:select "COUNT"})
                           :count)))

(defn get-item [request]
  (invoke-request :get-item
                  request
                  {:encode-attribs [:key]
                   :decode-attribs [:item]}))

(defn put-item [request]
  (invoke-request :put-item
                  request
                  {:encode-attribs [:item]}))

(defn delete-item [request]
  (invoke-request :delete-item request {:encode-attribs [:key]}))

(defn transact-write-items [request]
  (invoke-request
   :transact-write-items
   (update request
           :transact-items
           #(mapv (fn [m]
                    (->> m
                         (map
                          (fn [[k v]]
                            [k (update-with-codec codec/encode
                                                  v
                                                  [:Item :Key])]))
                         (into {})))
                  %))))

(defn batch-write-item [request]
  (invoke-request
   :batch-write-item
   (update request
           :request-items
           #(->> %
                 (mapv
                  (fn [[table-name requests]]
                    [table-name
                     (->> requests
                          (map (fn [m]
                                 (->> m
                                      (map
                                       (fn [[k v]]
                                         [k (update-with-codec codec/encode
                                                               v
                                                               [:Item :Key])]))
                                      (into {})))))]))
                 (into {})))))
